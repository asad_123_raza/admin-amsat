<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta NAME="robots" CONTENT="noindex,nofollow">

    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('images/favicon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('images/favicon.png') }}">


    <title>Admin Portal</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/master.css') }}">
    <script src="{{ mix('/js/master.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
    @yield('css')

</head>

<body class="hold-transition sidebar-mini">
    <div class="loading" style="display:none">
        <div class="spinner"></div>
    </div>
    <div class="wrapper">

        <!-- Navbar -->
        @include('layouts.top_header')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('layouts.left_menu')
        <!-- /.Main Sidebar Container -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    <i class=""></i> @yield('parentModuleTitle')
                    <small>@yield('childModuleTitle')</small>
                </h1>
                <ol class="breadcrumb" style="display: none;">
                    <li><a href="{{route('home')}}"><i class="fas fa-home"></i> Home</a></li>
                    @yield('parentModule')
                    @yield('childModule')
                    <li class="active">@yield('activeModule')</li>
                </ol>
            </section>
            @yield('content')
        </div>
        <!-- /.content-wrapper -->


        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    @yield('js')

</body>

</html>
