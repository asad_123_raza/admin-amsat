@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Edit Service
                            <a class="btn btn-primary" style="float:right;" href="{{ route('services.index') }}">
                                Back</a>
                        </h2>
                    </div>
                </div>
            </div>


            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->


            <form action="{{ route('services.update',$services->services_id) }}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="services_name" value="{{ $services->services_name }}"
                                   class="form-control"
                                   placeholder="Services Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Short Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_short_description"
                                      placeholder="Short Description">{{ $services->services_short_description }}</textarea>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Slug:</strong>
                            {{ Form::text('slug', $services->slug , array('placeholder'=>"Slug",'class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255') ) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_full_description"
                                      placeholder="Full Description">{{ $services->services_full_description }}</textarea>
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Upload a Featured Image:</strong>
                            {{ Form::file('services_image') }}

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <image width="30%" height="30%" src="{{$services->services_image_url}}"/>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>
        </div>
    </section>

@endsection