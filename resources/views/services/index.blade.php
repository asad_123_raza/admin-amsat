@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Services
                            @can('services-create')
                                <a class="btn btn-success" style="float:right;" href="{{ route('services.create') }}">
                                    Create New Service</a>
                            @endcan
                        </h2>
                    </div>
                    <div class="pull-right">

                    </div>
                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->

            <div class="row">
                <div class="col-md-12">
                    <table id="services_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Service Id</th>
                            <th>Image</th>
                            <th>Services Name</th>
                            <th>Short Description</th>
                            <th>Full Description</th>
                            <th>Slug</th>
                            <th>Created At</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </section>

    <script>

        $(function () {

            var table_name = "services_table";
            var url = "{{ URL::to('/get-all-services') }}";
            var coloumns = [
                {data: 'services_id', name: 'services_id'},
                {data: 'services_image_url', name: 'services_image_url'},
                {data: 'services_name', name: 'services_name'},
                {data: 'services_short_description', name: 'services_short_description'},
                {data: 'services_full_description', name: 'services_full_description'},
                {data: 'slug', name: 'slug'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'}
            ];
            var postdata = [];

            CreateDataTableIns(table_name, url, coloumns, postdata);
        });

        function deleteService(id) {
            var formData = {"_token": "{{ csrf_token() }}"};
            var url = "/services/" + id;
            ajax_call("DELETE", url, formData, function (res) {
                location.href = "";
            }, function (res) {
                var json = JSON.parse(res.responseText);
                console.log(json.body);
            });
        }
    </script>

@endsection