@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Add New Services
                            <a class="btn btn-primary" style="float:right;" href="{{ route('services.index') }}">
                                Back</a>
                        </h2>

                    </div>

                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->

            {{--@if ($errors->any())--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>Whoops!</strong> There were some problems with your input.<br><br>--}}
                    {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}


            <form action="{{ route('services.store') }}" method="POST">
                @csrf


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Service Name:</strong>
                            <input type="text" name="services_name" class="form-control" placeholder="Service Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Short Description:</strong>
                            <textarea class="form-control" style="" name="services_short_description"
                                      placeholder="Short Description"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_full_description"
                                      placeholder="Full Description"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Image Url:</strong>
                            <input type="text" name="services_image_url" class="form-control" placeholder="Image Url">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>

        </div>
    </section>

@endsection