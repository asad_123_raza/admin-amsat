<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = "customers";

    protected $fillable = [
        'customer_first_name', 'customer_last_name', 'customer_work_email', 'customer_job_role', 'customer_company_name', 'customer_industry', 'customer_query', 'fk_country', 'customer_phone_number', 'created_by', 'updated_by'
    ];
    public static $customerRules = [
        "general" => [
            'customer_first_name' => 'required',
            'customer_work_email' => 'required|email|unique:customers,customer_work_email',
            'customer_phone_number' => 'required',
        ]
    ];

    public static function addNewCustomer($input)
    {
        self::create($input);
        return true;
    }

    public static function getCustomersByFilters($filter)
    {
        $data = Self::orderBy('id', 'DESC');
        if (count($filter)) {
            // apply filter if any in future
        }
        return $data->get();
    }
}
