<div class="col-md-12 submit" >
    <div class='row'>
        @if(session()->has('message'))
            <div class="alert alert-danger">
                <strong>Alert - </strong> {{ session()->get('message') }}
            </div>
        @endif
    </div>
</div>

<div class="col-md-12 mt-2 submit">
    @if(session()->has('success'))
        <div class="alert alert-success">
            <strong>Success - </strong> {{ session()->get('success') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <strong>Alert - </strong> {{ session()->get('error') }}
        </div>
    @endif
</div>
<!-- left column -->
<div class="col-md-12">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your
            input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

</div>
<div class="col-md-12">
    <div class="alert alert-success" style="display: none;">
        <span class="message"></span>
    </div>
    <div class="alert alert-danger" style="display: none;">
        <span class="message"></span>
    </div>
</div>
