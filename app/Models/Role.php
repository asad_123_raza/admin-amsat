<?php

namespace App\Models;

use Spatie\Permission\Models\Role as spatieRole;

class Role extends spatieRole
{
    protected $guard_name = "web";
}