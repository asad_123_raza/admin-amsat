@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Add New Services
                            <a class="btn btn-primary" style="float:right;" href="{{ route('services.index') }}">
                                Back</a>
                        </h2>
                        <hr>

                    </div>

                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->

            {{--<form action="{{ route('services.store') }}" data-parsley-validate = '' files = true method="POST">--}}
                {!! Form::open(array('route' => 'services.store', 'data-parsley-validate' => '', 'files' => true)) !!}
                @csrf


                <div class="row card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Service Name:</strong>
                            <input type="text" name="services_name" class="form-control" placeholder="Service Name" required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Short Description:</strong>
                            <textarea class="form-control" style="" name="services_short_description"
                                      placeholder="Short Description" required></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Slug:</strong>
                            {{ Form::text('slug', null, array('placeholder'=>"Slug",'class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255') ) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_full_description"
                                      placeholder="Full Description" required></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Image:</strong>
                            {{ Form::file('services_image',array('required'=>"true")) }}
                            {{--<input type="text" name="services_image_url" class="form-control" placeholder="Image Url">--}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>

        </div>
    </section>

@endsection