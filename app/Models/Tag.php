<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;
    protected $table = "tags";

    protected $fillable = [
        'name','created_by','updated_by'
    ];

    public static $tagsRules = [
        "general" => [
            'name' => 'required|max:255'
        ]
    ];

    public function posts()
    {
    	return $this->belongsToMany('App\Models\Post','post_tag','fk_tag_id','fk_post_id');
    }
}
