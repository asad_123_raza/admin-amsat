<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Helpers\ResponseHandler;
use Log;
use DB;

class DemoController extends Controller
{

    function __construct()
    {

    }

    public static function getHome(){
        return view('website.demo.index');
    }
    public function getDemoRequestForm()
    {
        return view('website.demo.form');
    }

    public function postDemoRequest(Request $request)
    {
        ///dd($request->all());

        $validationErrors = Helper::validationErrors($request, Customer::$customerRules["general"]);

        if ($validationErrors) {
            return ResponseHandler::validationError($validationErrors);
        }

        try {
            Customer::addNewCustomer($request->all());
            return ResponseHandler::success(__('messages.request-demo.success'));
        } catch (\Exception $e) {
            session()->flash('alert-danger', $e->getMessage());
            return ResponseHandler::serverError($e);
        }

        //dd($request->all());
//        $validationErrors = Helper::validationErrors($request, Services::$servicesRules["general"]);
//        if ($validationErrors) {
//            return back()->withErrors($validationErrors)->withInput($request->all());
//        }
//        try {
//            $request->request->add(['created_by' => Auth::user()->id]);
//            Services::createService($request->all());
//            return redirect()->route('services.index')
//                ->with('success', __('messages.services.created'));
//        } catch (\Exception $e) {
//            Log::info($e);
//            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
//        }

    }


}