$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function ajax_call(method, url, data, success_function, error_function) {
    alert(method);
    $.ajax({
        type: method,
        url: url,
        dataType: "json",
        data: data
    }).then(success_function, error_function);
}
