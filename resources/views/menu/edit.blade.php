@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Edit MENU
                            <a class="btn btn-primary" style="float:right;" href="{{ route('menu.index') }}">
                                Back</a>
                        </h2>
                    </div>
                </div>
            </div>


            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->


            {!! Form::model($menu, ['route' => ['menu.update', $menu->id], 'method' => 'PUT','data-parsley-validate' => '', 'files' => true]) !!}

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Select Parent:</strong>
                        {{ Form::select('id_parent', ['' => 'Select menu'] + $parent_menus, null, ['class' => 'form-control'])}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Menu Name:</strong>
                        {{ Form::text('name', null, ["class" => 'form-control ']) }}
                        {{--<input type="text" name="name" class="form-control" placeholder="Menu Name">--}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Link Type:</strong>
                        {{ Form::select('link_type', $link_type, null, ['onchange' => "getLinksByValue(this.value)",'id'=>'link_type','class' => 'form-control']) }}

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{ Form::select('link_id', ['' => 'Select'],null, array("id" =>"link_id",'class' => 'form-control','style'=>'display:none')) }}
                        <input type="text" name="link_detail"  placeholder="Insert Link" id="link_detail" class="form-control" style ='display:none' >
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status:</strong>
                        {{ Form::select('is_active', $status, null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group">
                        <strong>Sort Order:</strong>
                        {{ Form::text('sort_order', null, ["class" => 'form-control ']) }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>


            </form>
        </div>
    </section>
@section('js')
    <script type="text/javascript">

        {{--$(function () {--}}
            {{--getProvincesByCountryId({{isset($mer_buss_setting->country)?$mer_buss_setting->country:null}})--}}
            {{--getCitiesByProvinceId({{isset($mer_buss_setting->province)?$mer_buss_setting->province:null}})--}}
        {{--});--}}
        getLinksByValue($("#link_type").val());

        function fileDropdown(dd_id, options, first_value,selected_value) {
            alert(dd_id);
            $("#link_detail").hide();
            $("#" + dd_id).find('option').remove();
            $("#" + dd_id).show();
            $("#" + dd_id).append(new Option(first_value, ''));
            $.each(options, function (key, value) {
                if(key == selected_value) {
                    $("#" + dd_id).append(new Option(value, key, true, true));
                } else {
                    $("#" + dd_id).append(new Option(value, key));
                }
            });
        }

        function getLinksByValue(value) {
            if (value == "page") {
                alert("page");
//                var formData = {};
//                var url = "/get-pages";
//                ajax_call("GET", url, formData, function (res) {
//                    fileDropdown('link_id', res.body, 'Select '+value);
//                }, function (res) {
//                    var json = JSON.parse(res.responseText);
//                    console.log(json.body);
//                });
            } else if (value == "service") {
                const link_id = '{{isset($menu->link_id)?$menu->link_id:null}}';
                var formData = {};
                var url = "/get-services";
                ajax_call("GET", url, formData, function (res) {
                    fileDropdown('link_id', res.body, 'Select ' + value,link_id);
                }, function (res) {
                    var json = JSON.parse(res.responseText);
                    console.log(json.body);
                });

            } else {
                $("#link_id").hide();
                $("#link_detail").show();
            }

        }
    </script>
@stop
@endsection