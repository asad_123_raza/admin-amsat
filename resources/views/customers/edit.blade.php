@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Edit Customer
                            <a class="btn btn-primary" style="float:right;" href="{{ route('services.index') }}">
                                Back</a>
                        </h2>
                    </div>
                </div>
            </div>


            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->


            <form action="{{ route('services.update',$services->services_id) }}" method="POST">
                @csrf
                @method('PUT')


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="services_name" value="{{ $services->services_name }}"
                                   class="form-control"
                                   placeholder="Services Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Short Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_short_description"
                                      placeholder="Short Description">{{ $services->services_short_description }}</textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Full Description:</strong>
                            <textarea class="form-control" style="height:150px" name="services_full_description"
                                      placeholder="Full Description">{{ $services->services_full_description }}</textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Image Url:</strong>
                            <input type="text" name="services_image_url" value="{{ $services->services_image_url }}"
                                   class="form-control"
                                   placeholder="Image Url">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>
        </div>
    </section>

@endsection