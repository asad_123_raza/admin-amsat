<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Services extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $primaryKey = 'services_id';
    protected $table = "services";

    protected $fillable = [
        'services_name', 'services_short_description', 'services_full_description', 'services_image_url','slug', 'created_by', 'updated_by'
    ];

    public static $servicesRules = [
        "general" => [
            'services_name' => 'required',
            'services_short_description' => 'required',
            'slug' => 'required',
        ]
    ];

    public static function getServicesList()
    {
        $services = self::latest()->paginate(5);
        return $services;
    }


    public static function getServicesByFilters($filter)
    {
        $data = Self::orderBy('services_id', 'DESC');
        if (count($filter)) {
            // apply filter if any in future
        }
        return $data->get();
    }


    public static function getServicesPluckList()
    {
        $services = self::pluck("services_name","services_id");
        return $services;
    }


    public static function createService($input)
    {
        self::create($input);
        return true;
    }

    public static function getServicesById($id)
    {
        $services = self::find($id);
        return $services;
    }

    public static function updateService($id, $update)
{
    $services = self::find($id);
    $services->update($update);
    return true;
}

    public static function deleteService($id, $user_id)
    {
        $services = self::find($id);
        $services->deleted_by = $user_id;
        $services->save();
        $services->delete();
    }

}
