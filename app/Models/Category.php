<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';

    protected $fillable = [
        'name','created_by','updated_by'
    ];
    public static $categoriesRules = [
        "general" => [
            'name' => 'required',
        ]
    ];

    public function posts()
    {
    	return $this->hasMany('App\Models\Post','fk_category_id','id');
    }
}
