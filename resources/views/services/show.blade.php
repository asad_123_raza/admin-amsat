@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Show Service
                            <a class="btn btn-primary" style="float:right;" href="{{ route('services.index') }}">
                                Back</a>
                        </h2>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Services Name:</strong>
                        {{ $services->services_name }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Services Short Description:</strong>
                        {{ $services->services_short_description }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Services Full Description:</strong>
                        {{ $services->services_full_description }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Services Slug:</strong>
                        {{ $services->slug }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Image:</strong>
                        <image  src="{{$services->services_image_url}}"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
