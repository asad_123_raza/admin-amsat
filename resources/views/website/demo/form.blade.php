@extends('website.layout.website')

@section('css')
    <style>
        .section {
            background: url(/images/footer-bg-form.jpg);
            background-size: cover;
            background-repeat: no-repeat;
        }

    </style>
@stop

@section('content')
    <div class="entry-content">
        <section id="contact" class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8 offset-md-2 ">
                            <h2 class="contact-heading text-center text-white">Ready to <span>Get Started?</span></h2>
                            <p class="contact-para text-center text-white">Our specialists are ready to tailor our
                                security service
                                solutions to fit the needs of your organization.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div role="form" class="wpcf7" id="wpcf7-f93-p89-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" role="alert" aria-live="polite"></div>

                            <!-- ALERTS STARTS HERE -->
                            <section>
                                <div class="row">
                                    @include('common.alerts')
                                </div>
                            </section>
                            <!-- ALERTS ENDS HERE -->

                            {{--Start--}}
                            {!! Form::open(array('name'=>'request_form','id'=>'request_form','url' => '/submit_demo_request')) !!}

                            <div style="display: none;">
                            </div>
                            <div class="col-md-8 offset-md-2">
                                <div class="row">
                                    <div class="col-md-6 text-white">
                                        <label> First Name:<span>*</span><br>
                                            {{ Form::text('customer_first_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                        </label>
                                    </div>
                                    <div class="col-md-6 text-white">
                                        <label> Last Name:<span>*</span><br>
                                        {{ Form::text('customer_last_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-white">
                                        <label> Work Email:<span>*</span><br>
                                            {{ Form::email('customer_work_email', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                        </label>
                                    </div>
                                    <div class="col-md-6 text-white ">
                                        <label> Job Role:<span>*</span><br>
                                            {{ Form::text('customer_job_role', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-white">
                                        <label> Country:<span>*</span><br>
                                            {{ Form::text('fk_country_id', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                        </label>
                                    </div>
                                    <div class="col-md-6 text-white">
                                        <label> Phone Number:<span>*</span><br>
                                            {{ Form::text('customer_phone_number', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-white">
                                        <label> INQUIRY:<span>*</span><br>
                                            {{ Form::textarea('customer_query', null, array('class' => 'form-control')) }}
                                            <p></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-white">
                                        <p class="terms">
                                            By submitting the form, you agree to the <a href="#">Terms of Use</a> and <a
                                                    href="#">Privacy Policy</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center text-white">
                                        {{ Form::submit('Submit Your Question', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <br>
                            <div class="succes_alert alert alert-success hide" style="display: none;" aria-hidden="true"
                                 role="alert">

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        function saveform() {


            var formData = $("#request_form").serializeArray();

            ajax_call("POST", "/submit_demo_request", formData, function (res) {
                $(".succes_alert").html('');
                $(".succes_alert").html(res.body);
                $(".succes_alert").removeClass("alert-danger");
                $(".succes_alert").addClass("alert-success");
                $(".succes_alert").show();
            }, function (res) {
                var json = JSON.parse(res.responseText);
                $(".succes_alert").html('');
                $(".succes_alert").html(json.body);
                $(".succes_alert").removeClass("alert-success");
                $(".succes_alert").addClass("alert-danger");
                $(".succes_alert").show();
            });
        }

        $(document).ready(function () {
            $('#request_form').submit(function (e) {

                e.preventDefault();
                var $form = $(this);

                // check if the input is valid
                if (!$form.valid()) return false;
                saveform();

            })
        });


    </script>

@endsection