@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Add New Menu
                            <a class="btn btn-primary" style="float:right;" href="{{ route('menu.index') }}">
                                Back</a>
                        </h2>

                    </div>

                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>


            <form action="{{ route('menu.store') }}" method="POST">
                @csrf
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Select Parent:</strong>
                            <select  class="form-control" name="id_parent" id="id_parent">
                                <option value='0'>Select menu</option>
                                @foreach($parent_menus as $key => $name)
                                    <option value='{{ $key }}'>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Menu Name:</strong>
                            <input type="text" name="name" class="form-control" placeholder="Menu Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Link Type:</strong>
                            <select onchange = "getLinksByValue(this.value)" class="form-control" name="link_type" id="link_type">
                                @foreach($link_type as $key => $name)
                                    <option value='{{ $key }}'>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            {{ Form::select('link_id', ['' => 'Select'],null, array("id" =>"link_id",'class' => 'form-control','style'=>'display:none')) }}
                            <input type="text" name="link_detail"  placeholder="Insert Link" id="link_detail" class="form-control" style ='display:none' >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Status:</strong>
                            <select class="form-control" name="is_active">
                                @foreach($status as $key => $name)
                                    <option {{ ($key == '1')?'selected':''}} value='{{ $key }}'>{{ $name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>Sort Order:</strong>
                            <input type="text" name="sort_order" class="form-control col-sm" placeholder="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>

        </div>
    </section>
@section('js')
    <script type="text/javascript">

        {{--$(function () {--}}
            {{--getProvincesByCountryId({{isset($mer_buss_setting->country)?$mer_buss_setting->country:null}})--}}
            {{--getCitiesByProvinceId({{isset($mer_buss_setting->province)?$mer_buss_setting->province:null}})--}}
        {{--});--}}
        getLinksByValue($("#link_type").val());

        function fileDropdown(dd_id, options, first_value) {
            $("#link_detail").hide();
            $("#" + dd_id).find('option').remove();
            $("#" + dd_id).show();
            $("#" + dd_id).append(new Option(first_value, ''));
            $.each(options, function (key, value) {
                $("#" + dd_id).append(new Option(value, key));
            });
        }

        function getLinksByValue(value) {
            if(value == "page"){
                alert("page");
//                var formData = {};
//                var url = "/get-pages";
//                ajax_call("GET", url, formData, function (res) {
//                    fileDropdown('link_id', res.body, 'Select '+value);
//                }, function (res) {
//                    var json = JSON.parse(res.responseText);
//                    console.log(json.body);
//                });
            }else if(value == "service"){
                var formData = {};
                var url = "/get-services";
                ajax_call("GET", url, formData, function (res) {
                    fileDropdown('link_id', res.body, 'Select '+value);
                }, function (res) {
                    var json = JSON.parse(res.responseText);
                    console.log(json.body);
                });

            }else{
                $("#link_id").hide();
                $("#link_detail").show();
            }


        }
    </script>
@stop
@endsection