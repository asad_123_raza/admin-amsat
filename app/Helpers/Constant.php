<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Helpers;

class Constant
{
    const HTTP_RESPONSE_STATUSES = [
        'success' => 200,
        'failed' => 400,
        'validationError' => 422,
        'authenticationError' => 401,
        'authorizationError' => 403,
        'serverError' => 500,
    ];

    const LINK_TYPES = [
        'link' => "Link",
        'page' => "Page",
        'service' => "Service",
    ];

    const MENU_STATUS = [
        '0' => "Inactive",
        '1' => "Active",
    ];
}
