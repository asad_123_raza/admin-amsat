<?php

return [
    'general' => [
        'failed' => 'Request Failed',
        'success' => 'Request Successful',
        'validation' => 'Validation Error',
        'crashed' => 'Something went wrong',
        'unauthenticated' => 'Authentication Failed',
        'unauthorized' => 'Authorization Failed',
    ],
    'roles' => [
        'created' => 'Role created successfully.',
        'updated' => 'Role updated successfully.',
        'deleted' => 'Role deleted successfully.'
    ],
    'services' => [
        'created' => 'Service created successfully.',
        'updated' => 'Service updated successfully.',
        'deleted' => 'Service deleted successfully.'
    ],
    'post' => [
        'created' => 'Post created successfully.',
        'updated' => 'Post updated successfully.',
        'deleted' => 'Post deleted successfully.'
    ],
    'request-demo' => [
        'success' => 'Thank you for your message. It has been sent.',
        'updated' => 'Post updated successfully.',
        'deleted' => 'Post deleted successfully.'
    ],
    'user' => [
        'profile_image_update' => 'Image uploaded successfully',
        'profile_update' => 'Profile data updated successfully.',
        'deleted' => 'Post deleted successfully.'
    ],
    'menu' => [
        'created' => 'Menu created successfully.',
        'updated' => 'Menu updated successfully.',
        'deleted' => 'Menu deleted successfully.'
    ],
];
