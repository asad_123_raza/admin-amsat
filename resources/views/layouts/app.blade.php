<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('images/favicon-logo.svg') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('images/favicon-logo.svg') }}">

    <title>Amsat Admin Portal</title>
    <!-- <title>{{ config('app.name', 'Laravel') }}</title> -->

    <link rel="stylesheet" href="{{ mix('css/master.css') }}">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm topLoginBar">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Admin Portal
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            {{--                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                            {{--<a class="nav-link" href="{{ route('signin') }}">{{ __('Login') }}</a>--}}
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            {{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                            {{--@if(request()->getHost() == config("app.domains.cockpit"))--}}
                            {{--@else--}}
{{--                            <a class="nav-link" href="{{ route('signup') }}">{{ __('Register') }}</a>--}}
                            {{--@endif--}}

                        </li>
                        @endif
                        @else
                        <!-- <li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>
                            <li><a class="nav-link" href="{{ route('roles.index') }}">Manage Role</a></li>
                            <li><a class="nav-link" href="{{ route('products.index') }}">Manage Product</a></li> -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->email }} dedsd <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 loginContentBox">
            @yield('content')
        </main>
    </div>
    <script src="{{ mix('/js/master.js') }}"></script>

    @yield('js')
</body>

</html>
