
<nav class="navbar navbar-expand-sm bg-dark text-white">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ URL::to('/') }}">HOME</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="#">SERVICES</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="#">ABOUT US</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="#">BLOGS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="#">NEWS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="#">CONTACT US</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ URL::to('/request_demo') }}">REQUEST DEMO</a>
        </li>
    </ul>
</nav>
