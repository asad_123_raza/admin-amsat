<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = $this->defaultPermissions();


        foreach ($permissions as $perms) {
            Permission::firstOrCreate($perms);
        }


        $roles = Role::get()->map(function($item){


            switch ($item->name)
            {
                case 'Admin':

                    $permissions = Permission::pluck('id','id')->all();
                    $item->syncPermissions($permissions);
                    $this->command->info($item->name.' granted all the permissions');

                break;
                case 'Customer':
//                    $item->givePermissionTo('view-reports');
//                    $this->command->info($item->name.' granted all the permissions');

                break;

                case 'Author':
                    $item->givePermissionTo('services-list');
                    $item->givePermissionTo('services-create');
                    $item->givePermissionTo('services-edit');
                    $item->givePermissionTo('services-delete');

                    $item->givePermissionTo('page-list');
                    $item->givePermissionTo('page-create');
                    $item->givePermissionTo('page-edit');
                    $item->givePermissionTo('page-delete');

                    $this->command->info($item->name.' granted all the permissions');

                break;


                default:
                    $this->command->info('No roles found in your system');
                }
        });



    }

    public function defaultPermissions()
    {

        return [

            ['name' => 'role-list', 'guard_name' => 'web','group'=>'Role Management' ],
            ['name' => 'role-create', 'guard_name' => 'web','group'=>'Role Management' ],
            ['name' => 'role-edit', 'guard_name' => 'web','group'=>'Role Management' ] ,
            ['name' => 'role-delete', 'guard_name' => 'web','group'=>'Role Management'] ,

            ['name' => 'services-list', 'guard_name' => 'web','group'=>'Services Management'] ,
            ['name' => 'services-create', 'guard_name' => 'web','group'=>'Services Management'] ,
            ['name' => 'services-edit', 'guard_name' => 'web','group'=>'Services Management' ],
            ['name' => 'services-delete', 'guard_name' => 'web','group'=>'Services Management' ],

            ['name' => 'user-list', 'guard_name' => 'web','group'=>'Users Management' ],
            ['name' => 'user-create', 'guard_name' => 'web','group'=>'Users Management' ],
            ['name' => 'user-edit', 'guard_name' => 'web','group'=>'Users Management' ] ,
            ['name' => 'user-delete', 'guard_name' => 'web','group'=>'Users Management'] ,

            ['name' => 'menue-list', 'guard_name' => 'web','group'=>'Menu Management' ],
            ['name' => 'menue-create', 'guard_name' => 'web','group'=>'Menu Management' ],
            ['name' => 'menue-edit', 'guard_name' => 'web','group'=>'Menu Management' ] ,
            ['name' => 'menue-delete', 'guard_name' => 'web','group'=>'Menu Management'] ,

            ['name' => 'page-list', 'guard_name' => 'web','group'=>'Pages Management' ],
            ['name' => 'page-create', 'guard_name' => 'web','group'=>'Pages Management' ],
            ['name' => 'page-edit', 'guard_name' => 'web','group'=>'Pages Management' ] ,
            ['name' => 'page-delete', 'guard_name' => 'web','group'=>'Pages Management'] ,

            ['name' => 'blog-list', 'guard_name' => 'web','group'=>'Blog Management' ],
            ['name' => 'blog-create', 'guard_name' => 'web','group'=>'Blog Management' ],
            ['name' => 'blog-edit', 'guard_name' => 'web','group'=>'Blog Management' ] ,
            ['name' => 'blog-delete', 'guard_name' => 'web','group'=>'Blog Management'] ,

            ['name' => 'category-list', 'guard_name' => 'web','group'=>'Category Management' ],
            ['name' => 'category-create', 'guard_name' => 'web','group'=>'Category Management' ],
            ['name' => 'category-edit', 'guard_name' => 'web','group'=>'Category Management' ] ,
            ['name' => 'category-delete', 'guard_name' => 'web','group'=>'Category Management'] ,

            ['name' => 'post-list', 'guard_name' => 'web','group'=>'Post Management' ],
            ['name' => 'post-create', 'guard_name' => 'web' ,'group'=>'Post Management'],
            ['name' => 'post-edit', 'guard_name' => 'web' ,'group'=>'Post Management'] ,
            ['name' => 'post-delete', 'guard_name' => 'web','group'=>'Post Management'] ,

            ['name' => 'tag-list', 'guard_name' => 'web','group'=>'Tag Management' ],
            ['name' => 'tag-create', 'guard_name' => 'web' ,'group'=>'Tag Management'],
            ['name' => 'tag-edit', 'guard_name' => 'web' ,'group'=>'Tag Management'] ,
            ['name' => 'tag-delete', 'guard_name' => 'web','group'=>'Tag Management'] ,

            ['name' => 'customer-list', 'guard_name' => 'web' ,'group'=>'Customer Management'],
            ['name' => 'customer-create', 'guard_name' => 'web' ,'group'=>'Customer Management'],
            ['name' => 'customer-edit', 'guard_name' => 'web','group'=>'Customer Management' ] ,
            ['name' => 'customer-delete', 'guard_name' => 'web','group'=>'Customer Management'] ,

            ['name' => 'menu-list', 'guard_name' => 'web','group'=>'Menu Management' ],
            ['name' => 'menu-create', 'guard_name' => 'web','group'=>'Menu Management' ],
            ['name' => 'menu-edit', 'guard_name' => 'web','group'=>'Menu Management' ] ,
            ['name' => 'menu-delete', 'guard_name' => 'web','group'=>'Menu Management'] ,
        ];
    }
}
