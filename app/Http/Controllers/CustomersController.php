<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Helpers\ResponseHandler;
use Log;
use DB;
use URL;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkPermission('customer-list');
        return view('customers.index');
    }

    public function getCustomers(Request $request)
    {
        try {
            $filter = $request->all();
            $customers = Customer::getCustomersByFilters($filter);
            return $this->makeDatatable($customers);
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }

    public function makeDatatable($dasta)
    {


        return \DataTables::of($dasta)
            ->addColumn('id', function ($rowdata) {
                $return = '<td><a target="_blank" href="' . URL::to("/customers/$rowdata->id") . '">' . $rowdata->id . '</a></td>';
                return $return;
            })
            ->addColumn('customer_first_name', function ($rowdata) {
                $return = $rowdata->customer_first_name;
                return $return;
            })
            ->addColumn('action', function ($rowdata) {
                $return = '';
                $return .='<a href="' .route('customers.show',$rowdata->id).'"><i class="fas fa-search-plus"></i></a>';
                $return .='<a href="' .route('customers.show',$rowdata->id).'"><i class="fas fa-edit"></i></a>';
                $return .='<a href="' .route('customers.show',$rowdata->id).'"><i class="fas fa-trash"></i></a>';
//                $return .='<a class="btn btn-info" href="'.route('customers.show',$rowdata->id).'"><i class="fas fa-eye"></a>';
//                $return .='<a class="btn btn-primary" href="'.route('customers.show',$rowdata->id).'"><i class="fas fa-eye"></a>';
                return $return;
            })
            ->rawColumns(['name', 'id', 'roles', 'status', 'action'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkPermission('services-create');
        return view('services.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('services-create');
        $validationErrors = Helper::validationErrors($request, Services::$servicesRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }

        try {
            $request->request->add(['created_by' => Auth::user()->id]);
            Services::create($request->all());
            return redirect()->route('services.index')
                ->with('success', __('messages.services.created'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Services $services
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkPermission('customer-list');
        $customer = Customer::find($id);
        return view('customers.show', compact('customer'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services $services
     * @return \Illuminate\Http\Response
     */
    public function edit($services_id)
    {
        $this->checkPermission('services-edit');
        $services = Services::find($services_id);
        return view('services.edit', compact('services'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Services $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $services_id)
    {
        $this->checkPermission('services-edit');
        $validationErrors = Helper::validationErrors($request, Services::$servicesRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }
        try {
            $services = Services::find($services_id);
            $request->request->add(['updated_by' => Auth::user()->id]);
            $services->update($request->all());
            return redirect()->route('services.index')
                ->with('success', __('messages.services.updated'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services $services
     * @return \Illuminate\Http\Response
     */
    public function destroy($services_id)
    {
        $this->checkPermission('services-delete');
        DB::beginTransaction();
        try {
            $services = Services::find($services_id);
            $services->deleted_by = Auth::user()->id;
            $services->save();
            $services->delete();
            DB::commit();
            return redirect()->route('services.index')
                ->with('success', __('messages.services.deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            return redirect()->route('services.index')->withErrors(__('messages.general.crashed'));
        }
    }
}