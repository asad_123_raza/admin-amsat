@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <h2>Menu
                            @can('services-create')
                                <a class="btn btn-success" style="float:right;" href="{{ route('menu.create') }}"> Create New Menu</a>
                            @endcan
                        </h2>
                    </div>
                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->

            <div class="row">
                <div class="col-md-12">
                    <table id="menu_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Menu Id</th>
                            <th>Name</th>
                            <th>Link Type</th>
                            <th>Link Slug</th>
                            <th>Status</th>
                            <th>Sort Order</th>
                            <th>Created At</th>
                            <th width="90px">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>
        function deleteMenu(id){
            
            var formData = {"_token": "{{ csrf_token() }}"};
            var url = "/menu/"+id;
            ajax_call("DELETE", url, formData, function (res) {
                location.href = "";
            }, function (res) {
                var json = JSON.parse(res.responseText);
                console.log(json.body);
            });
        }

        $(function () {

            var table_name = "menu_table";
            var url = "{{ URL::to('/get-menus') }}";
            var coloumns = [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'link_type', name: 'link_type'},
                {data: 'link_detail', name: 'link_detail'},
                {data: 'is_active', name: 'is_active'},
                {data: 'sort_order', name: 'sort_order'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'}
            ];
            var postdata = [];

            CreateDataTableIns(table_name, url, coloumns, postdata);
        });


    </script>

@endsection