<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Helpers\ResponseHandler;
use Log;
use DB;
use URL;
use App\Helpers\Constant;

class MenuController extends Controller
{

    function __construct()
    {

    }

    public function index()
    {
        $this->checkPermission('menu-list');
        return view('menu.index');
    }

    public function getMenus(Request $request)
    {
        try {
            $filter = $request->all();
            $menu = Menu::getMenusByFilters($filter);
            return $this->makeDatatable($menu);
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }

    public function makeDatatable($data)
    {
        return \DataTables::of($data)
            ->addColumn('id', function ($rowdata) {
                $return = '<td><a target="_blank" href="' . URL::to("/menu/$rowdata->id") . '">' . $rowdata->id . '</a></td>';
                return $return;
            })
            ->addColumn('name', function ($rowdata) {
                $return = $rowdata->name;
                return $return;
            })
            ->addColumn('is_active', function ($rowdata) {
                $return = "";
                if($rowdata->is_active)
                {
                    $return .= "<label class='badge badge-success'>Active</label>";
                }else{
                    $return .= "<label class='badge badge-info'>In Active</label>";
                }
                return $return;
            })
            ->addColumn('link_detail', function ($rowdata) {
                $return = "";
                if($rowdata->link_type == "service")
                {
                    $serviceObj = Services::getServicesById($rowdata->link_id);
                    $return .= "$serviceObj->slug";
                }else if($rowdata->link_type == "link"){
                    $return .= "$rowdata->link_detail";
                }
                return $return;
            })
            ->addColumn('created_at', function ($rowdata) {
                $return = date('M j, Y h:ia', strtotime($rowdata->created_at));
                return $return;
            })
            ->addColumn('action', function ($rowdata) {
                $return = '';
                $return .='&nbsp;<a href="' .route('menu.edit',$rowdata->id).'"><i class="fas fa-edit"></i></a>';
                $return .='&nbsp;<a href="#" onclick="deleteMenu('.$rowdata->id.');return false;"><i class="fas fa-trash"></i></a>';
               return $return;
            })
            ->rawColumns(['name','is_active','id','action'])
            ->make(true);
    }


    public function create()
    {
        $this->checkPermission('menu-create');
        $link_type = Constant::LINK_TYPES;
        $status = Constant::MENU_STATUS;
        $parent_menus = Menu::getParentMenu();
        return view('menu.create',compact("link_type","status",'parent_menus'));
    }

    public function store(Request $request)
    {

        $this->checkPermission('menu-create');
        $validationErrors = Helper::validationErrors($request, Menu::$menuRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }
        try {
            $request->request->add(['created_by' => Auth::user()->id]);
            Menu::createMenu($request->all());
            return redirect()->route('menu.index')
                ->with('success', __('messages.menu.created'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }

    }

    public function show($id)
    {
        $this->checkPermission('menu-list');
        $menu = Menu::getMenuById($id);
        return view('menu.show', compact('menu'));
    }

    public function edit($id)
    {
        $this->checkPermission('menu-edit');
        $menu = Menu::getMenuById($id);
        $link_type = Constant::LINK_TYPES;
        $status = Constant::MENU_STATUS;
        $parent_menus = Menu::getParentMenu();
        return view('menu.edit', compact('parent_menus','menu','link_type','status'));
    }

    public function update(Request $request, $id)
    {
        $this->checkPermission('menu-edit');
        $validationErrors = Helper::validationErrors($request, Menu::$menuRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }
        try {
            $request->request->add(['updated_by' => Auth::user()->id]);
            Menu::updateMenu($id, $request->all());
            return redirect()->route('menu.index')
                ->with('success', __('messages.menu.updated'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        $this->checkPermission('menu-delete');
        DB::beginTransaction();
        try {
            Menu::deleteMenu($id, Auth::user()->id);
            DB::commit();
            return ResponseHandler::success(__('messages.menu.deleted'));
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }
}