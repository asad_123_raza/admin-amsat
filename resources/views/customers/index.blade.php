@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                    </div>
                </div>
            </div>

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(array('id' => 'select-form','method'=>'POST', 'class'=>'form-inline ', 'url' => '/customer/delete/seleted')) !!}

                    {!! Form::close() !!}
                    <table id="customers_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Customer Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Company</th>
                            <th>Customer Query</th>
                            <th>Industry</th>
                            <th>Phone</th>
                            <th>Creted At</th>
                            <th width="900px">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>

        $(function () {

            var table_name = "customers_table";
            var url = "{{ URL::to('/get-customers') }}";
            var coloumns = [
                {data: 'id', name: 'id'},
                {data: 'customer_first_name', name: 'customer_first_name'},
                {data: 'customer_last_name', name: 'customer_last_name'},
                {data: 'customer_work_email', name: 'customer_work_email'},
                {data: 'customer_job_role', name: 'customer_job_role'},
                {data: 'customer_company_name', name: 'customer_company_name'},
                {data: 'customer_query', name: 'customer_query'},
                {data: 'customer_industry', name: 'customer_industry'},
                {data: 'customer_phone_number', name: 'customer_phone_number'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'}

            ];
            var postdata = [];

            CreateDataTableIns(table_name, url, coloumns, postdata);
        });
    </script>

@endsection