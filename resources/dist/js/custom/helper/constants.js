App.Constants = {

    /*
    *****************************************************
    ******** PHP <> JS CONFIG VARIABLES MAPPING ********
    ****************************************************
    RESTRICTED AREA
    PLEASE DON'T SET VALUE FOR ANY undefined VARIABLE FROM HERE
    VALUE FOR FOLLOWING VARIABLES IS SET FROM config_mapping.blade.php
    */

    BASE_URL : undefined,
    API_HOST : undefined,
    API_ASSETS_URL : undefined,
    API_ENDPOINTS: undefined,
    REQUEST_HEADERS: undefined,
    HTTP_CODES: undefined,
    SUCCESS: undefined,
    BAD_REQUEST: undefined,
    UNAUTHORISED: undefined,
    INPROCESSABLE: undefined,
    CAR_STATUS: undefined,
    CAR_VAlUES: undefined,
    MARGIN: undefined,
    INDEX_MARGIN: undefined,
    MARGIN_TYPE: undefined,
    INDEX_TYPE: undefined,
    IMAGE_ANGLES: undefined,
    USER: undefined,
    DEALER: undefined,
    DEFAULT_CURRENCY_NAME: undefined,
    INSPECTION_REPORT_CLASSIFICATION: undefined,

    // ACL

    isAdmin: undefined,
    isAdminOnly: undefined,
    isAccountManager: undefined,
    isJuniorAccountManager: undefined,
    isSeniorAccountManager: undefined,
    isDealer: undefined,
    isInspector: undefined,

    /*
    ****************************************************
    ************** USER DEFINED CONSTANTS **************
    ****************************************************
     */

    //TODO
    HTTP_SCHEME : "http://",
    ALERT_DIV_TIMEOUT: 5000,
    DROPDOWN_EMPTY_OPTION : '<option value=""></option>',
    ENGINE_CYLINDER_SUFFIX : 'Cyl',
    ENGINE_CAPACITY_SUFFIX : 'L',
    ENGINE_HORSEPOWER_SUFFIX : 'hp'
};
