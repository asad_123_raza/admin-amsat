<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
        <span class="logo-mini">
            <img src="{{ asset('images/logo.png') }}" alt="Amsat" class="brand-image" style="width:30%;height:10%;">
        </span>
        {{--<span class="logo-lg">--}}
        {{--<img src="{{ asset('images/black.png') }}" alt="Amsat" class="brand-image" style="">--}}
        {{--</span>--}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ URL::to('dashboard') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                @can('user-list')
                    <li class="nav-item has-treeview {{ (Request::is('users') || Request::is('roles')) ? 'active menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-cog"></i>
                            <p>
                                User Management
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
                                <a href="{{ URL::to('users') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Users</p>
                                </a>
                            </li>

                            @can('role-list')
                                <li class="nav-item {{ Request::is('roles') ? 'active' : '' }}">
                                    <a href="{{ URL::to('roles') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Roles</p>
                                    </a>
                                </li>
                            @endcan

                            @can('customer-list')
                                <li class="nav-item {{ Request::is('customer') ? 'active' : '' }}">
                                    <a href="{{ URL::to('customers') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Customers</p>
                                    </a>
                                </li>
                            @endcan

                        </ul>
                    </li>

                @endcan

                @can('blog-list')
                    <li class="nav-item has-treeview {{ (Request::is('blog') ) ? 'active menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-blog"></i>
                            {{--<i class="fas fa-blog"></i>--}}
                            <p>
                                Blog
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('post-list')
                            <li class="nav-item {{ Request::is('posts') ? 'active' : '' }}">
                                <a href="{{ URL::to('posts') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    {{--<i class="fas fa-sim-card"></i>--}}
                                    <p>Post</p>
                                </a>
                            </li>
                            @endcan
                            @can('category-list')
                                <li class="nav-item {{ Request::is('categories') ? 'active' : '' }}">
                                    <a href="{{ URL::to('categories') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        {{--<i class="fas fa-album-collection"></i>--}}
                                        <p>Category</p>
                                    </a>
                                </li>
                            @endcan

                            @can('tag-list')
                                <li class="nav-item {{ Request::is('tags') ? 'active' : '' }}">
                                    <a href="{{ URL::to('tags') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        {{--<i class="fas fa-tags"></i>--}}
                                        <p>Tag</p>
                                    </a>
                                </li>
                            @endcan

                        </ul>
                    </li>

                @endcan

                @if(auth()->user()->can('services-list') )
                    <li class="nav-item {{ Request::is('services') ? 'active' : '' }}">
                        <a href="{{ URL::to('services') }}" class="nav-link">
                            <i class="fas fa-business-time nav-icon"></i>
                            {{--<i class="fas fa-business-time"></i>--}}
                            <p>Services</p>
                        </a>
                    </li>

                @endif

                @can('menu-list')
                    <li class="nav-item {{ Request::is('services') ? 'active' : '' }}">
                        <a href="{{ URL::to('menu') }}" class="nav-link">
                            <i class="fas fa-business-time nav-icon"></i>
                            {{--<i class="fas fa-business-time"></i>--}}
                            <p>Menu Management</p>
                        </a>
                    </li>

                @endcan


                @if(auth()->user()->can('page-list') )
                    <li class="nav-item {{ Request::is('pages') ? 'active' : '' }}">
                        <a href="{{ URL::to('pages') }}" class="nav-link">
                            <i class="fas fa-pager nav-icon"></i>
                            {{--<i class="fas fa-pager"></i>--}}
                            <p>Pages</p>
                        </a>
                    </li>

                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
