@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Menu Details
                            <a class="btn btn-primary" style="float:right;" href="{{ route('menu.index') }}">
                                Back</a>
                        </h2>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Menu Name:</strong>
                        {{$menu->name}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Link Type:</strong>
                        {{$menu->link_type}}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status:</strong>
                        @if($menu->is_active)
                            Active
                        @else
                            In Active
                        @endif

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Sort Order:</strong>
                        {{$menu->sort_order}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Created At:</strong>
                        {{ date('M j, Y h:ia', strtotime($menu->created_at))}}
</div>
</div>
</div>


</div>
</section>
@endsection
