@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">

            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->
            <div class="row">
                <dl class="col-md-4">
                    <dt>Created At:</dt>
                    <dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
                </dl>
                <dl class="col-md-2">
                    &nbsp;
                </dl>
                <dl class="col-md-4">
                    <dt>Last Updated:</dt>
                    <dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
                </dl>
            </div>

            <div class="row">
                {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT','data-parsley-validate' => '', 'files' => true]) !!}

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Title:</strong>
                        {{ Form::text('title', null, ["class" => 'form-control ']) }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Slug:</strong>
                        {{ Form::text('slug', null, ['class' => 'form-control']) }}
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Category:</strong>
                        {{ Form::select('fk_category_id', $categories, null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tags:</strong>
                        {{ Form::select('tags[]', $tags, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple']) }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Upload a Featured Image:</strong>
                        {{ Form::file('featured_img') }}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <image width="30%" height="30%" src="{{$post->image}}"/>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Post Body:</strong>
                        {{ Form::textarea('body', null, ['id'=>'post_body','class' => 'form-control']) }}
                    </div>
                </div>


                <div class="well">
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Html::linkRoute('posts.show', 'Cancel', array($post->id), array('class' => 'btn btn-danger btn-block')) !!}
                        </div>
                        <div class="col-sm-6">
                            {{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
                        </div>
                    </div>

                </div>

                {!! Form::close() !!}
            </div>    <!-- end of .row (form) -->
        </div>
        </div>
    </section>
    <script src="{{ URL::asset('ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">

        $('.select2-multi').select2();
        $('.select2-multi').select2().val({!! json_encode($post->tags()->allRelatedIds()) !!}).trigger('change');
        CKEDITOR.replace('post_body');
    </script>
@endsection

