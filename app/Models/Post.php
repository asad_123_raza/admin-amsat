<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = "posts";

    protected $fillable = [
        'title', 'body','slug','image','fk_category_id','created_by','updated_by'
    ];

    public static $postRules = [
        "general" => [
            'title'            => 'required|max:255',
            'slug'             => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'fk_category_id'   => 'required|integer',
            'body'             => 'required'
        ],
        "slug_match" =>[
            'title' => 'required|max:255',
            'fk_category_id' => 'required|integer',
            'body'  => 'required'
        ]
    ];

    public function category()
    {
    	return $this->belongsTo('App\Models\Category','fk_category_id','id');
    }

    public function tags()
    {
    	return $this->belongsToMany('App\Models\Tag','post_tag','fk_post_id','fk_tag_id');
    }

}