App.Ajax = {

    submitForm: function (formId, onSuccess, onFailure, formClass = false) {

        var options = {
            cache: false,
            headers: App.Helpers.getApiHeaders(true, false),
            beforeSend: function () {
                $('.loader').addClass('loading');
            },
            beforeSerialize: function () { //data alteration before submit
                // you can use callback here
            },
            success: function (data) {
                $('.loader').removeClass('loading');
                if(onSuccess){
                    onSuccess(data);
                }
            },

            error: function (data) {
                $('.loader').removeClass('loading');
                App.Helpers.alertError(data);
                if(onFailure){
                    onFailure(data);
                }
            }
        };

        if (formClass) {
            $('.' + formId).ajaxForm(options);
        } else {
            $('#' + formId).ajaxForm(options);
        }
    },

    get: function (endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions) {
        App.Ajax.__send("GET", endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions);
    },

    post: function (endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions) {
        App.Ajax.__send("POST", endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions);
    },

    delete: function (endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions) {
        App.Ajax.__send("DELETE", endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions);
    },

    __send: function (method, endPoint, data, onSuccess, onFailure, $apiRoute, additionalOptions) {

        let url;

        if ($apiRoute === undefined) {
            url = App.Helpers.generateApiURL(endPoint);
            url += App.Helpers.containParams(url) ? "&" : "?";
            url += "access_token=" + localStorage.getItem('JWT');
        } else {
            url = App.Helpers.generateWebURL(endPoint)
        }

        let options = {

            url: url,
            type: method,
            data: data,
            headers: App.Helpers.getApiHeaders(),
            contentType: 'application/x-www-form-urlencoded',

            beforeSend: function () {
                $('.loader').addClass('loading');
            },

            success: function (data, textStatus, xhr) {
                $('.loader').removeClass('loading');
                onSuccess(data.body, data.message);
            },

            error: function (xhr, textStatus, errorThrown) {
                $('.loader').removeClass('loading');
                let response = xhr.responseJSON;
                App.Helpers.showError(response.message);
                onFailure(response);
            },

            complete: function (response) {
                $('.loader').removeClass('loading');
            }
        };

        for (var propertyName in additionalOptions) {
            options[propertyName] = additionalOptions[propertyName];
        }

        $.ajax(options);
    }
}
