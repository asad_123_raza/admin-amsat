<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Tag;
use App\Models\Category;
use App\Helpers\Helper;
use Log;
use DB;
use Session;
use Purifier;
use Image;


class PostController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create')->withCategories($categories)->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // validate the data
        $this->checkPermission('post-create');
        $validationErrors = Helper::validationErrors($request, Post::$postRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }

        try {
            // store in the database
            $post = new Post;
            $post->title = $request->title;
            $post->slug = $request->slug;
            $post->fk_category_id = $request->fk_category_id;
            $post->body = $request->body;

            if ($request->hasFile('featured_img')) {
                $image = $request->file('featured_img');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $file_path = '/post_images/';
                $resize["width"] = 800;
                $resize["height"] = 400;
                Helper::uploadImage($image, $file_path, $filename, $resize);
                $post->image = $file_path . $filename;
            }
            $post->save();
            $post->tags()->sync($request->tags, false);
            return redirect()->route('posts.index', $post->id)->with('success', __('messages.post.created'));

        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with("category")->find($id);
        $path = Helper::getStaticPath();
        $post->image = $path.$post->image;
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in the database and save as a var
        $post = Post::with("category")->find($id);
        $categories = Category::all();
        $cats = array();
        foreach ($categories as $category) {
            $cats[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;
        }

        $path = Helper::getStaticPath();
        $post->image = $path.$post->image;

        return view('posts.edit')->withPost($post)->withCategories($cats)->withTags($tags2);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the data
        $this->checkPermission('post-edit');

        $post = Post::find($id);

        if ($request->input('slug') == $post->slug) {
            $validationErrors = Helper::validationErrors($request, Post::$postRules["slug_match"]);

        } else {
            $validationErrors = Helper::validationErrors($request, Post::$postRules["general"]);
        }

        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }

        try {

            // Save the data to the database
            $post = Post::find($id);

            $post->title = $request->input('title');
            $post->slug = $request->input('slug');
            $post->fk_category_id = $request->input('fk_category_id');
            $post->body = $request->input('body');
            if ($request->hasFile('featured_img')) {
                $image = $request->file('featured_img');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $file_path = '/post_images/';
                $resize["width"] = 800;
                $resize["height"] = 400;
                Helper::uploadImage($image, $file_path, $filename, $resize);
                $post->image = $file_path . $filename;
            }

            $post->save();

            if (isset($request->tags)) {
                $post->tags()->sync($request->tags);
            } else {
                $post->tags()->sync(array());
            }
            return redirect()->route('posts.show', $post->id)->with('success', __('messages.post.updated'));

        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->tags()->detach();
        $post->delete();

        Session::flash('success', 'The post was successfully deleted.');
        return redirect()->route('posts.index');

        $this->checkPermission('post-delete');
        DB::beginTransaction();
        try {
            $post = Post::find($id);
            $post->deleted_by = Auth::user()->id;
            $post->save();
            $post->tags()->detach();
            $post->delete();
            DB::commit();
            return redirect()->route('posts.index')
                ->with('success', __('messages.post.deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            return redirect()->route('posts.index')->withErrors(__('messages.general.crashed'));
        }

    }
}
