const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
// .sass('resources/sass/app.scss', 'public/css');


var paths = {
    'plugins': './resources/plugins/',
    'dist': './resources/dist/',
    'ionicons': './node_modules/ionicons/',
}


mix.sass('resources/sass/app.scss', 'public/css');

mix.styles([
    paths.plugins + 'fontawesome-free/css/all.min.css',
    paths.plugins + 'tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
    paths.plugins + 'datatables/css/jquery.dataTables.min.css',
    paths.plugins + 'datatables/css/select.dataTables.min.css',
    paths.plugins + 'datatables/css/buttons.dataTables.min.css',
    paths.plugins + 'icheck-bootstrap/icheck-bootstrap.min.css',
    paths.plugins + 'overlayScrollbars/css/OverlayScrollbars.min.css',
    paths.plugins + 'daterangepicker/daterangepicker.css',
    paths.plugins + 'summernote/summernote-bs4.css',
    paths.dist + 'css/sweetalert.min.css',
    paths.dist + 'css/adminlte.min.css',
    paths.dist + 'css/select2.min.css',
    paths.dist + 'css/custom.css',
    'public/css/app.css',
], 'public/css/master.css').version();

mix.scripts([
    paths.plugins + "jquery/jquery.min.js",
    paths.plugins + "jquery-ui/jquery-ui.min.js",
    paths.plugins + "bootstrap/js/bootstrap.bundle.min.js",
    paths.plugins + "datatables/jquery.dataTables.min.js",
    paths.plugins + "datatables/dataTables.select.min.js",
    paths.plugins + "datatables/dataTables.buttons.min.js",
    paths.plugins + "datatables-bs4/js/dataTables.bootstrap4.min.js",
    paths.plugins + "datatables-responsive/js/dataTables.responsive.min.js",
    paths.plugins + "datatables-responsive/js/responsive.bootstrap4.min.js",
    paths.plugins + "moment/moment.min.js",
    paths.plugins + "daterangepicker/daterangepicker.js",
    paths.plugins + "tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js",
    paths.plugins + "overlayScrollbars/js/jquery.overlayScrollbars.min.js",
    paths.dist + "js/sweetalert.min.js",
    paths.dist + "js/adminlte.js",
    paths.dist + "js/select2.min.js",
    paths.dist + "js/repeater.js",
    paths.dist + "js/jquery.validate.min.js",
    paths.dist + "js/jquery.inputmask.bundle.min.js",
    //paths.ionicons + 'dist/cjs/ionicons.cjs.js',
    'public/js/helper.js',
], 'public/js/master.js').version();
