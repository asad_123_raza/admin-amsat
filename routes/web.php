<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Authentication Routes & Registration Routes
Auth::routes();
Route::get('get-services', 'ServicesController@getServices');
//Route::get('get-pages', 'PagesController@getPages');

$backendRoutes = function () {

    Route::get('/', 'HomeController@dashboard')->middleware('auth');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => ['auth']], function () {

        Route::get('profile/{user}', 'UserController@profile')->name('profile.view');
        Route::post('profile/{user}', 'UserController@profileUpdate')->name('profile.edit');
        Route::post('profile_image_upload/{user}', 'UserController@profileImageUpload')->name('profile.upload');
        
        Route::get('/dashboard', 'HomeController@dashboard');
        Route::resource('roles', 'RoleController');
        Route::resource('users', 'UserController');
        Route::resource('services', 'ServicesController');
        Route::get('get-all-services', 'ServicesController@getAllServices');


        // Blog Routes
        Route::resource('categories', 'CategoryController', ['except' => ['create']]);
        Route::resource('tags', 'TagController', ['except' => ['create']]);
        Route::resource('posts', 'PostController');

        // Customers
        Route::resource('customers', 'CustomersController');
        Route::get('get-customers', 'CustomersController@getCustomers');

        // Menu
        Route::resource('menu', 'MenuController');
        Route::get('get-menus', 'MenuController@getMenus');

    });
};

$frontendRoutes = function () {
    Route::get('/', 'DemoController@getHome');
    Route::get('/request_demo', 'DemoController@getDemoRequestForm');
    Route::post('/submit_demo_request', 'DemoController@postDemoRequest');
};


Route::group(['domain' => Config::get('app.domains.admin'), 'middleware' => []], $backendRoutes);
Route::group(['domain' => Config::get('app.domains.website'), 'middleware' => []], $frontendRoutes);