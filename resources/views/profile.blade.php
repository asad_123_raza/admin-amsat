@extends('layouts.admin')

@section('content')
    <!-- /.row -->
    <div class="card  ">
        <div class="card-header">
            <h3 class="card-title">
                My Profile
            </h3>
        </div>
        <!-- ALERTS STARTS HERE -->
        <section>
            <div class="row">
                @include('common.alerts')
            </div>
        </section>
        <!-- ALERTS ENDS HERE -->
        <div class="card-body">
            {{--            <h4>Left Sided</h4>--}}
            <form id="profile_image_form" action="{{ route('profile.upload',$user->id) }}" accept-charset="UTF-8"
                  data-parsley-validate method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-5 col-sm-3">

                        <div class="col-sm-2 col-md-2">
                            <img width="2000%" src="{{$user->image}}" alt=""
                                 class="img-rounded img-responsive" style="border: 2px solid #73AD21;border-radius: 50%;"/>
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <strong>Change picture:</strong>
                            {{ Form::file('profile_image',array('id'=>'profile_image')) }}
                        </div>
                        <br>
                        <div class="col-sm-2 col-md-2">
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </div>

            </form>

                    <div class="col-7 col-sm-9">
                        <form id="profile_form" action="{{ route('profile.edit',$user->id) }}" accept-charset="UTF-8">

                        <div class="tab-content" id="vert-tabs-tabContent">

                            <div class="tab-pane fade show active" id="vert-tabs-profile" role="tabpanel"
                                 aria-labelledby="vert-tabs-profile-tab">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-primary">

                                            @csrf
                                            <div class="card-body">

                                                <input type="hidden" name="id" value="{{Auth::user()->id}}"/>

                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" name="name" id="name" required="required"
                                                           value="{{Auth::user()->name}}"
                                                           minlength="3"
                                                           maxlength="30"
                                                           class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" name="email" value="{{Auth::user()->email}}"
                                                           id="email"
                                                           minlength="10"
                                                           maxlength="30"
                                                           required="required"
                                                           class="form-control">
                                                </div>

                                                {{--<div class="form-group">--}}
                                                    {{--<label for="phone">Phone</label>--}}
                                                <!-- <input type="text" name="phone"
                                                           required="required"
                                                           minlength="10"
                                                           maxlength="13"
                                                           value="{{Auth::user()->country_code.Auth::user()->phone}}"
                                                           id="phone"
                                                           class="form-control"> -->

                                                    {{--{!! Form::text('phone', Auth::user()->country_code.Auth::user()->phone, array('minlength'=>"10",'maxlength' => '14', 'data-inputmask' => "'mask': ['99999-9999999']",'data-mask' => 'data-mask','class' => 'form-control','id'=>"phone",'required' => 'required')) !!}--}}
                                                {{--</div>--}}

                                                <div class="form-group">
                                                    <button type="submit" id="send_form" class="btn btn-success">Update
                                                    </button>
                                                </div>

                                            </div>
            </form>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

    </div>
    </div>

    </div>
    </div>
    </div>
    </div>
    <!-- /.card -->
    </div>
    <!-- /.card -->

@section('js')
    <script type="text/javascript">
        $(":input").inputmask();

        function saveform(formid) {

            var formData = $("#"+formid).serializeArray()
            var url = '<?php json_encode(config('app.domains.admin').'/profile/' .  Auth::user()->id ) ?>';
            ajax_call("POST", url, formData, function (res) {
                showSuccessMessage(res.body);
            }, function (res) {
                var json = JSON.parse(res.responseText);
                showErrorMessage(json.body);
            });
        }

        $(document).ready(function () {
            var formid = "profile_form";
            $('#'+formid).validate();
            $('#'+formid).submit(function (e) {

                e.preventDefault();
                var $form = $(this);

//                var phone = $("#phone").val();
//                phone = phone.replace(/_/g, '');
//
//                if (phone != '' && phone.length < 13) {
//                    var validator = $('#'+formid).validate();
//                    validator.showErrors({
//                        "phone": "Please enter complete phone number."
//                    });
//                    return false;
//                }

                // check if the input is valid
                if (!$form.valid()) return false;
                saveform(formid);

            });
            $('#profile_image_form').submit(function (e) {


                var files = $('#profile_image')[0].files;

                // Check file selected or not
                if(files.length > 0 ) {
                    return true;
                }else{
                    showErrorMessage({'image':"Please choose image"});
                    return false;
                }
                e.preventDefault();

            });
        });


        function showSuccessMessage(message) {
            // $('#bank_details_form_button').html('Save');
            $(".alert-danger").hide();
            $(".submit").hide();
            $('.alert-success').html('');
            $('.alert-success').html("<strong>Success - </strong>"+message);
            $(".alert-success").show();
            hideMessage();
        }

        function showErrorMessage(messages) {
            // $('#bank_details_form_button').html('Save');
            $('.alert .message').html('');
            $(".alert-success").hide();
            $(".submit").hide();
            var errors = '';
            $.each(messages, function (key, value) {
                errors += '<p>' + value + '</p>';
            });
            $('.alert .message').html("<strong>Alert - </strong>" + errors);
            $(".alert-danger").show();
            hideMessage();
        }

        function hideMessage() {
            window.scrollTo(0, 0);
            setTimeout(function () {
                $(".alert").hide();
                $('.alert .message').html('');
            }, 5000);
        }

    </script>
    @endsection
@stop
