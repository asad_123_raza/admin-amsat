function CreateDataTableIns(table_id, ajax_url, coloumns, postdata, buttons=[]) {

    if (buttons.length > 0) {
        var buttons = buttons;
    } else {
        var buttons = [];
    }

    var oTable = $('#' + table_id).DataTable({
        dom: 'Bfrtip',
        bFilter: false,
        processing: true,
        serverSide: true,
        columnDefs: [{
            orderable: false,
            targets: 0
        }],
        buttons: buttons,
        ajax: {
            url: ajax_url,
            data: postdata
        },
        columns: coloumns
    });

}
