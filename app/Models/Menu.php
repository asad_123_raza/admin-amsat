<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = "menu";

    protected $fillable = [
        'id_parent', 'name', 'link_type','link_id', 'link_detail', 'is_active', 'sort_order', 'created_by', 'updated_by'
    ];

    public static $menuRules = [
        "general" => [
            'name' => 'required',
        ]
    ];

    public static function getMenusByFilters($filter)
    {
        $data = Self::orderBy('id', 'DESC');
        if (count($filter)) {
            // apply filter if any in future
        }
        return $data->get();
    }

    public static function getParentMenu(){
        return Self::where('is_active', '1')
                    ->where('id_parent', 0)->pluck('name','id')->toArray();
    }

    public static function createMenu($input)
    {
        $input["link_id"] = is_null($input["link_id"]) ? 0 : $input["link_id"];
        self::create($input);
        return true;
    }


    public static function getMenuById($id)
    {
        $menu = self::find($id);
        return $menu;
    }

    public static function updateMenu($id, $update)
    {
        $menu = self::find($id);
        $update["link_id"] = is_null($update["link_id"]) ? 0 : $update["link_id"];
        $menu->update($update);
        return true;
    }

    public static function deleteMenu($id, $user_id)
    {
        $menu = self::find($id);
        $menu->deleted_by = $user_id;
        $menu->save();
        $menu->delete();
    }
}
