<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Http\Controllers;

use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Helpers\ResponseHandler;
use Log;
use DB;
use URL;

class ServicesController extends Controller
{

    function __construct()
    {

    }

    public function index()
    {

        $this->checkPermission('services-list');
        $services = Services::getServicesList();
        $path = Helper::getStaticPath();
        return view('services.index', compact('services','path'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function getAllServices(Request $request)
    {
        try {
            $filter = $request->all();
            $services = Services::getServicesByFilters($filter);
            return $this->makeDatatable($services);
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }

    public function makeDatatable($data)
    {
        return \DataTables::of($data)
            ->addColumn('services_id', function ($rowdata) {
                $return = '<td><a target="_blank" href="' . URL::to("/services/$rowdata->services_id") . '">' . $rowdata->services_id . '</a></td>';
                return $return;
            })

            ->addColumn('services_image_url', function ($rowdata) {
                $return = '<td><image width="100%" src="'.Helper::getStaticPath().$rowdata->services_image_url.'"/></td>';
                return $return;
            })

            ->addColumn('created_at', function ($rowdata) {
                $return = date('M j, Y h:ia', strtotime($rowdata->created_at));
                return $return;
            })
            ->addColumn('action', function ($rowdata) {
                $return = '';
                $return .='&nbsp;<a href="' .route('services.edit',$rowdata->services_id).'"><i class="fas fa-edit"></i></a>';
                $return .='&nbsp;<a href="#" onclick="deleteMenu('.$rowdata->services_id.');return false;"><i class="fas fa-trash"></i></a>';
                return $return;
            })
            ->rawColumns(['services_image_url','services_id','action'])
            ->make(true);
    }




    public function create()
    {
        $this->checkPermission('services-create');
        return view('services.create');
    }

    public function store(Request $request)
    {

        $this->checkPermission('services-create');
        $validationErrors = Helper::validationErrors($request, Services::$servicesRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }
        try {
            $request->request->add(['created_by' => Auth::user()->id]);
            if ($request->hasFile('services_image')) {
                $image = $request->file('services_image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $file_path = '/services_images/';
                $resize["width"] = 179;
                $resize["height"] = 179;
                Helper::uploadImage($image, $file_path, $filename, $resize);
                $request->request->add(['services_image_url' => $file_path . $filename]);
            }

            Services::createService($request->all());
            return redirect()->route('services.index')
                ->with('success', __('messages.services.created'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }

    }

    public function show($services_id)
    {
        $this->checkPermission('services-list');
        $services = Services::getServicesById($services_id);
        $path = Helper::getStaticPath();
        $services->services_image_url = $path.$services->services_image_url;
        return view('services.show', compact('services'));
    }

    public function edit($services_id)
    {
        $this->checkPermission('services-edit');
        $services = Services::getServicesById($services_id);
        $path = Helper::getStaticPath();
        $services->services_image_url = $path.$services->services_image_url;
        return view('services.edit', compact('services'));
    }

    public function update(Request $request, $services_id)
    {
        $this->checkPermission('services-edit');
        $validationErrors = Helper::validationErrors($request, Services::$servicesRules["general"]);
        if ($validationErrors) {
            return back()->withErrors($validationErrors)->withInput($request->all());
        }
        try {
            $request->request->add(['updated_by' => Auth::user()->id]);
            if ($request->hasFile('services_image')) {
                $image = $request->file('services_image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $file_path = '/services_images/';
                $resize["width"] = 179;
                $resize["height"] = 179;
                Helper::uploadImage($image, $file_path, $filename, $resize);
                $request->request->add(['services_image_url' => $file_path . $filename]);
            }
            Services::updateService($services_id, $request->all());
            return redirect()->route('services.index')
                ->with('success', __('messages.services.updated'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }
    }

    public function destroy($services_id)
    {
        $this->checkPermission('services-delete');
        DB::beginTransaction();
        try {
            Services::deleteService($services_id, Auth::user()->id);
            DB::commit();
            return redirect()->route('services.index')
                ->with('success', __('messages.services.deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            return redirect()->route('services.index')->withErrors(__('messages.general.crashed'));
        }
    }

    public function getServices()
    {
        try{
            $services = Services::getServicesPluckList();
            return ResponseHandler::success($services,'');
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }


}