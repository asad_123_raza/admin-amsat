<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- SEARCH FORM -->
{{--  <form class="form-inline ml-3">--}}
{{--   <div class="input-group input-group-sm">--}}
{{--      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
{{--      <div class="input-group-append">--}}
{{--        <button class="btn btn-navbar" type="submit">--}}
{{--          <i class="fas fa-search"></i>--}}
{{--        </button>--}}
{{--      </div>--}}
{{--   </div>--}}
{{--  </form>--}}

<!-- Right navbar links -->
    <ul class="navbar-nav ml-auto topCustomNav">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user-lock"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="/profile/{{ Auth::user()->id }}" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="/dist/img/user.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                {{ Auth::user()->name ?? '' }}
                            </h3>
                            <p class="text-md text-muted">{{ Auth::user()->email ?? '' }}</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>

                <div class="dropdown-divider"></div>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="dropdown-item dropdown-footer"><i class=""></i>Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        {{--<li class="nav-item MobileNone loggedLink">--}}
          {{--<span>Logged in as:</span>--}}
          {{--<a href="http://localhost:8000/admin-u-1.html" target="_blank">admin</a>--}}
          {{--<a href="/profile/{{ Auth::user()->id }}" class="">--}}
              {{--@if(Auth::user()->name != '')--}}
                    {{--{{ Auth::user()->name }}--}}
              {{--@else {{ Auth::user()->email }}--}}
              {{--@endif</a>--}}

        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link logoutLink">--}}
                {{--<i class=""></i>Logout--}}
            {{--</a>--}}
            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                {{--@csrf--}}
            {{--</form>--}}
        {{--</li>--}}
    </ul>
</nav>
