<?php
/**
 * @Author: Asad Raza
 * @Dated: 15-Oct-2020
 *
 */

namespace App\Helpers;


use App\Helpers\Constant;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use DateTime;
use File;

class Helper
{

    static function validationErrors($request, $rules)
    {
        if( is_array( $request ) )
        {
            $formData = $request;
        }
        else
        {
            $formData = $request->all();
        }

        $validator = Validator::make($formData, $rules);

        if ($validator->fails()) {
            return $validator->errors();
        } else {
            return false;
        }
    }

    static function hasPermission($permission){

       if( Auth::user()->hasPermissionTo($permission)){
           return true;
       }else{
           return false;
       }
    }

    static function saveWithQualityImage($main_image_path,$resized_image_path,$image_name,$width=0,$height=0,$quality=null) {

        if(config('app.upload_to_s3')){
            // Code when upload to s3
        }
        else{
            // Creating Directory If It Does Not Exist
            File::exists($resized_image_path) or File::makeDirectory($resized_image_path);
            $img = Image::make($main_image_path.'/'.$image_name);
            $img->save($resized_image_path.'/'.$image_name,$quality);
        }
    }

    static function resizeImage($main_image_path,$resized_image_path,$image_name,$width=0,$height=0,$quality=null) {

        if(config('app.upload_to_s3')){
            // Code when upload to s3
        }
        else{
            $img = Image::make($main_image_path);
        }
        if(($width > $img->width() && $height > $img->height()) || ($width == 0 || $height == 0)) {
            $width	= $img->width();
            $height	= $img->height();
        }
        if(config('app.upload_to_s3')){
            // Code when upload to s3
        }
        else{
            // Creating Directory If It Does Not Exist
            File::exists($resized_image_path) or File::makeDirectory($resized_image_path);
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save($resized_image_path.'/'.$image_name);
        }

    }

    static function uploadImage($image ,$destinationPath ,$imageName ,$resizing =[],$quality="") {


        if(config('app.upload_to_s3')){
                // Code when upload to s3
        }
        else{
            $basePath	= Config('app.domains.static_path');
            $finalPath	=  $basePath.$destinationPath;
            File::exists($finalPath) or File::makeDirectory($finalPath, $mode = 0755, $recursive = true, $force = false);
            if($quality!="" && count($resizing) == 0){
                $width = 0;
                $height = 0;
                $sourcePath =  $image;
                self::savewithqualityImage($sourcePath,$finalPath,$imageName,$width,$height,$quality);
            }elseif(count($resizing) > 0 ) {
                $width  = $resizing['width'] ;
                $height = $resizing['height'] ;
                $sourcePath = $image;
                self::resizeImage($sourcePath,$finalPath,$imageName,$width,$height);
            } else {
                $image->move($finalPath, $imageName);
            }
        }
    }

    static function getStaticPath () {
        $basePath = "http://".Config('app.domains.static');
        return  $basePath;
    }

}
