<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as spatiePermission;

class Permission extends spatiePermission
{
    protected $guard_name = "web";
}