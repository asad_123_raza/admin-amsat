<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('country_id');
            $table->integer('fk_region')->default(0);
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('flag_file')->nullable();
            $table->enum('is_ship', ['1', '0'])->default('0');
            $table->string('currency_code');
            $table->string('fips_code');
            $table->integer('iso_numeric');
            $table->float('north');
            $table->float('south');
            $table->float('east');
            $table->float('west');
            $table->string('capital');
            $table->string('continent_name');
            $table->string('continent');
            $table->string('languages');
            $table->string('iso_alpha3');
            $table->string('geoname_id');
            $table->enum('is_state_required', ['1', '0'])->default('0');
            $table->enum('is_postcode_required', ['1', '0'])->default('0');
            $table->string('country_code',20)->default('0');
            $table->integer('priority')->default('0');
            $table->integer('sort_order')->default('0');
            $table->double('vat', 13,2)->default('0.00');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->boolean('is_default')->default(0);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('deleted_by')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}

