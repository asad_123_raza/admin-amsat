<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use App\Helpers\Helper;
use App\Helpers\ResponseHandler;
use DB;
use Hash;
use Auth;
use Log;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function profile($id)
    {

        if(Auth::user()->id != $id){
            return abort(403);
        }
        $user = User::find($id);
        if($user->image == ""){
            $user->image = "http://placehold.it/380x500";
        }else{
            $path = Helper::getStaticPath();
            $user->image = $path.$user->image;
        }
        return view('profile',compact('user'));

    }

    public function profileImageUpload(Request $request, User $user){

        try {
            if ($request->hasFile('profile_image')) {
                $image = $request->file('profile_image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $file_path = '/profile_images/';
                $resize["width"] = 800;
                $resize["height"] = 400;
                Helper::uploadImage($image, $file_path, $filename, $resize);
                $user->image = $file_path . $filename;
            }
            $user->save();
            return redirect()->route('profile.view',array('user' => $user->id))
                ->with('success', __('messages.user.profile_image_update'));
        } catch (\Exception $e) {
            Log::info($e);
            return back()->withErrors(__('messages.general.crashed'))->withInput($request->all());
        }


    }
    public function profileUpdate(Request $request, User $user)
    {

        $validationErrors = Helper::validationErrors($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validationErrors) {
            return ResponseHandler::validationError($validationErrors);
        }

        try {
            $user->fill([
                'name' => $request->name,
                'email' => $request->email
            ]);
            
            $user->save();
            return ResponseHandler::success(__('messages.user.profile_update'));
        } catch (\Exception $e) {
            return ResponseHandler::serverError($e);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);
        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();


        return view('users.edit',compact('user','roles','userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }


        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}