@extends('layouts.admin')


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Create New Post
                            <a class="btn btn-primary" style="float:right;" href="{{ route('posts.index') }}">
                                Back</a>
                        </h2>
                        <hr>

                    </div>

                </div>
            </div>
            <!-- ALERTS STARTS HERE -->
            <section>
                <div class="row">
                    @include('common.alerts')
                </div>
            </section>
            <!-- ALERTS ENDS HERE -->
            {!! Form::open(array('route' => 'posts.store', 'data-parsley-validate' => '', 'files' => true)) !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Title:</strong>
                        {{ Form::text('title', null, array('class' => 'form-control ', 'required' => '', 'maxlength' => '255')) }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Slug:</strong>
                        {{ Form::text('slug', null, array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255') ) }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Category:</strong>
                        <select class="form-control" name="fk_category_id">
                            @foreach($categories as $category)
                                <option value='{{ $category->id }}'>{{ $category->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tags:</strong>
                        <select class="form-control select2-multi" name="tags[]" multiple="multiple">
                            @foreach($tags as $tag)
                                <option value='{{ $tag->id }}'>{{ $tag->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Upload a Featured Image:</strong>
                        {{ Form::file('featured_img') }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Post Body:</strong>
                        {{ Form::textarea('body', null, array('id'=>'post_body','class' => 'form-control')) }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <script src="{{ URL::asset('ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">
        $('.select2-multi').select2();
        CKEDITOR.replace( 'post_body' );
    </script>
@endsection








